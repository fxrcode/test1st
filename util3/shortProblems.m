%% solution to Assignment 1 of MIT: intro to MATLAB

% homework 1, problems 1 through 7

% 1
a = 10;
b = 2.5e23;
c = 2 + 3i;
d = exp(j*2*pi/3);

%2
aVec = [3.14 15 9 26];
bVec = [2.71; 8; 28; 182];
cVec = [5:-0.2:-5];