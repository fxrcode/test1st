/*
 *     SocialLedge.com - Copyright (C) 2013
 *
 *     This file is part of free software framework for embedded processors.
 *     You can use it and/or distribute it as long as this copyright header
 *     remains unmodified.  The code is free for personal use and requires
 *     permission to use in a commercial product.
 *
 *      THIS SOFTWARE IS PROVIDED "AS IS".  NO WARRANTIES, WHETHER EXPRESS, IMPLIED
 *      OR STATUTORY, INCLUDING, BUT NOT LIMITED TO, IMPLIED WARRANTIES OF
 *      MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE APPLY TO THIS SOFTWARE.
 *      I SHALL NOT, IN ANY CIRCUMSTANCES, BE LIABLE FOR SPECIAL, INCIDENTAL, OR
 *      CONSEQUENTIAL DAMAGES, FOR ANY REASON WHATSOEVER.
 *
 *     You can reach the author of this software at :
 *          p r e e t . w i k i @ g m a i l . c o m
 */

/**
 * @file
 * @brief This is the application entry point.
 */
#include <stdio.h>      // printf() / scanf()
#include "io.hpp"       // Board sensors
#include "utilities.h"  // delay_ms()
#include "LPC17xx.h"
#include "wireless.h"
#include "stdlib.h"
#include "soft_timer.hpp"

void set_led(const int sw);

bool ping_receiver(char addr, mesh_packet_t &pkt);

void send_packet(char addr, short int xval, short int yval, mesh_packet_t &pkt, bool bEnable);

int main(void)
{
    const int ON = 1;
    const int OFF = 0;

    /* Setup Switch debounce timer */
    SoftTimer swTimer(200);
    swTimer.reset(200);

    LD.setNumber(0);    // Clear display

    // Set Switches 0 and 1 to input
    LPC_GPIO1->FIODIR &= ~(1 << 9); //P1.9 to input
    LPC_GPIO1->FIODIR &= ~(1 << 10); //P1.10 to input


    // Set P1_0 to output (LED0-3)
    LPC_GPIO1->FIODIR |= (3 << 0);
    LPC_GPIO1->FIODIR |= (1 << 4);
    LPC_GPIO1->FIODIR |= (1 << 8);

    char addr = 200;
    mesh_packet_t pkt;

    // forever loop
    while(1)
    {

        // wait for switch before starting control
        while(!(LPC_GPIO1->FIOPIN & (1 << 9)));
        set_led(ON);
        bool started = true;
        bool bEnable = false;
        int switchValue;
        delay_ms(2000);     // delay so control loop doesn't immediately exit




        // Control Loop
        while(started)
        {

            //UART
            LPC_UART0->THR = 5;

            // When timer expires, status Proximity Sensor Switch
            if (swTimer.expired())
            {
                /* Get Switch Values */
                switchValue = LPC_GPIO1->FIOPIN;

                /* Proxmity Sensor Enable/Disable */
                if ((switchValue & (1 << 10)) == (1 << 10)) //SW active HIGH
                {
                    /* Reset timer */
                    swTimer.reset(200);

                    /* Enable/Disable */
                    if (bEnable)
                    {
                        bEnable = false;
                        LPC_GPIO1->FIOSET = (1 << 1); //HIGH = OFF
                    }
                    else
                    {
                        bEnable = true;
                        LPC_GPIO1->FIOCLR = (1 << 1); //LOW = ON
                    }
                }
            }


            // Get XVAL and YVAL from accelerometer
            short int xval = AS.getX();
            short int yval = AS.getY();
            // Send Wireless Packet
            send_packet(addr, xval, yval, pkt, bEnable);

            delay_ms(10); // 100 Hz

            // Check switch0 Status
            bool sw0_stat = !!(LPC_GPIO1->FIOPIN & (1 << 9));
            if (sw0_stat)  // Switch closed, disable LEDs and exit control loop
            {
                set_led(OFF);
                started = false;
                LD.setNumber(0);
                delay_ms(2000); // delay so it doesn't reenter control loop
            }

        } // end forever loop

    }
    return -1;
}

// Function Turns ON/OFF all 4 LEDs
void set_led(const int sw)
{
    if (sw==1) {
        //LPC_GPIO1->FIOCLR = (1 << 0) | (1 << 1) | (1 << 4) | (1 << 8);
        LPC_GPIO1->FIOCLR = (1 << 0);

    }
    else {
        //LPC_GPIO1->FIOSET = (1 << 0) | (1 << 1) | (1 << 4) | (1 << 8);
        LPC_GPIO1->FIOSET = (1 << 0);
    }
}

// Function to ping target receiver
bool ping_receiver(char addr, mesh_packet_t &pkt)
{
    bool acked = false;
    while (!acked) {
        if (!(wireless_send(200, mesh_pkt_ack, NULL, 0, 3)))
            printf("Wireless ping failed to send");
        if (wireless_get_ack_pkt(&pkt, 1000))
        {
            printf("Node response: ");
            for (int i=0; i < pkt.info.data_len; i++)
                putchar(pkt.data[i]);
                printf("\n");
            }
            else {
                printf("  No ACK\n");
                LD.setNumber(255);
            }
    }
    return true;
}

// Function to build command packet and transmit
void send_packet(char addr, short int xval, short int yval, mesh_packet_t &pkt, bool bEnable)
{
    //mesh_packet_t pkt;
    int hops = 0;
    int8_t sensor_en;

    int8_t direction = xval / 8;
    int8_t throttle = yval / 8;
    if (bEnable)
        sensor_en = 1;
    else
        sensor_en = 0;



    // Set Direction Limit
    if (direction > 100)
        direction = 100;
    else if (direction < -100)
        direction = -100;
    // Set Throttle Limit
    if (throttle > 100)
        throttle = 100;
    else if (throttle < -100)
        throttle = -100;
    // Calculate Checksum
    int8_t checksum = 0 - (0xCA + direction + throttle + sensor_en);
    int8_t header = 0xCA;

    // Form wireless packet (header, throttle, direction, checksum)
    wireless_form_pkt(&pkt, addr, mesh_pkt_nack, 0,
            5,
            &header,     sizeof(header),
            &throttle,   sizeof(throttle),
            &direction,  sizeof(direction),
            &sensor_en , sizeof(sensor_en),
            &checksum,   sizeof(checksum));
    wireless_send_formed_pkt(&pkt);
    //
    // Display throttle value, absolute value
    int throttle_display = abs(throttle);
    LD.setNumber(throttle_display);
    //
    // Print status out serial port
    printf("  Accelerometer XVAL: %#x\n", direction);
    printf("  Accelerometer YVAL: %#x\n", throttle);
    printf("  Sensor Enable: %#x\n", sensor_en);
    printf("  Checksum          : %#x\n\n", checksum);
}
