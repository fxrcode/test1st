#include <stdio.h>
#include <stdlib.h>
#include "lpc_sys.h"
#include "LPC17xx.h"
#include "inttypes.h"
#include "utilities.h"
#include "uart2.hpp"
#include "storage.hpp"
#include "i2c_base.hpp"
#include "i2c2.hpp"
#include "L4_IO/fat/disk/sd.h"
#include "spi1.h"
#include "io.hpp"

void pulse_high()
{
    LPC_GPIO0->FIOSET = 1 << 16; // pulse clock high
}
void pulse_low()
{
    LPC_GPIO0->FIOCLR = 1 << 16; // pulse clock low
}
void clear_Screen()
{
    pulse_high();
    pulse_low();
    spi1_exchange_byte(' ');
    pulse_high();
    delay_ms(100);
    pulse_low();
    spi1_exchange_byte(0xFE);
    pulse_high();
    pulse_low();
    spi1_exchange_byte(0x51);
    pulse_high();
    pulse_low();
    delay_ms(100);
}
/*
 *  Function to send a character to LCD
 *  @param x    The character to send
 */
void to_lcd(char x)
{
    spi1_exchange_byte(x);
    pulse_high();
    pulse_low();
}
/*
 *  Configures LCD
 */
void start_lcd(void)
{
    const int DIVISOR = 254;
    const int MULTIPLIER = 9;
    const int SCK = 15;

    const int CPOL = 6;
    const int CHPA = 7;

    spi1_init();

    LPC_SSP1->CPSR = DIVISOR; //set divisor
    LPC_SSP1->CR0 |= (1 << MULTIPLIER); //set the multiplier
    LPC_GPIO0->FIODIR = (1 << SCK); //set direction
    LPC_SSP1->CR0 |= (1 << CPOL); //CPOL = 1
    LPC_SSP1->CR0 |= (1 << CHPA); //CPHA = 1
    LPC_GPIO0->FIOSET = (1 << SCK); //set high
}
void lcd_string(char mystring[])
{

    for (unsigned int i = 0; i < sizeof(mystring); i = i + 1)
    {
        to_lcd(mystring[i]);
    }

}
void lcd(void* p)
{
    const int PIN = 28; //p1.26
    LPC_GPIO1->FIODIR |= (1 << PIN); // Output
    delay_ms(100);
    LPC_GPIO1->FIOSET = (1 << PIN); // HIGH
    LPC_GPIO1->FIOCLR = (1 << PIN); // LOW
    delay_ms(100);

    start_lcd();

    while (1)
    {
        clear_Screen();
        pulse_high();
        pulse_low();
        spi1_exchange_byte(0xFE);
        delay_ms(100);
        pulse_high();
        pulse_low();
        spi1_exchange_byte(0x45);
        delay_ms(100);
        pulse_high();
        pulse_low();

        spi1_exchange_byte(0x00);
        delay_ms(100);

        pulse_high();
        pulse_low();

        vTaskDelay(5000);
    }
}


int main(void)
{

    char init_file[10]="Track1\n";
    LPC_SC->PCONP |= (3 << 24); //Enabling UART 2/3 (need only one)
    LPC_PINCON->PINSEL0 &= ~(15 << 0); //Clear PINSEL0
    LPC_PINCON->PINSEL0 |= (1 << 20); //Set PINSEL0 to UART 3
    LPC_PINCON->PINSEL0 |= (1 << 22);

    Uart2& u2 = Uart2::getInstance();
    u2.init(9600); //Set Baud Rate to 9600


    Storage::write("1:track1.txt",init_file,10,0);

    const int PIN = 28; //p1.26
    LPC_GPIO1->FIODIR |= (1 << PIN); // Output
    delay_ms(100);
    LPC_GPIO1->FIOSET = (1 << PIN); // HIGH
    LPC_GPIO1->FIOCLR = (1 << PIN); // LOW
    delay_ms(100);

    start_lcd();
    char input_rpm;
    //char *uart_rpm=input_rpm;

    while (1)
    {
        /*
        pulse_high();
        pulse_low();
        input_rpm=spi1_exchange_byte(0xFF);
        pulse_high();
        pulse_low();
        spi1_exchange_byte(0x84);
        delay_ms(1000);
         */

        //Initialization
        int vehiclespeed=0, vehiclerpm=0;
        char input_rpm[20]="010c", input_speed[20]="010d", output_buffer[20];
        char rpm[20], speed[20];
        char *uart_RPM=input_rpm, *uart_speed=input_speed, *uart_output=output_buffer;
        u2.flush();


        //Retrieve RPM
        //printf("Input character: \t");
        //scanf("%s",uart_input1);
        u2.putline(uart_RPM,30); // etc.
        printf("\n uart_input:%s\n",uart_RPM);
        delay_ms(1500);
        u2.gets(uart_output,20,1000);
        printf("%s\n", uart_output);

        vehiclerpm = ((strtol(&uart_output[10],0,16)*256)+strtol(&uart_output[13],0,16))/4;
        sprintf(rpm,"RPM\t%d\n",vehiclerpm);
        Storage::append("1:track1.txt",rpm,20,0);
        printf("RPM: %d\n", vehiclerpm);
        u2.flush();

        u2.putline(uart_speed,30); // etc.
        delay_ms(1500);
        u2.gets(uart_output,20,1000);
        printf("%s\n", uart_output);
        vehiclespeed = strtol(&uart_output[10],0,16);
        printf("Speed: %d\n", vehiclespeed);
        sprintf(speed,"Speed\t%d\n",vehiclespeed);
        Storage::append("1:track1.txt",speed,20,0);

    }
}
